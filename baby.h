#ifndef BABY_H
#define BABY_H

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <locale>
#include <cstdlib>
#include <cmath>
#include <climits>
#include <bitset>

using namespace std;


class baby{


public:

    baby();
    ~baby();

    void printMem();
    void fetch();
    void initialise();
    void loadFile(string filename);
    string endianSwap(string binaryValue);
    //int binToDec(int n);

    string dectoBin(int dec);
    string getMem(int p){

        return mem[p];

    }

    void setMem(string m, int p){

        mem[p] = m;

    }

    string getAcc(){

        return acc;

    }

    void setAcc(string a){

        acc = a;

    }

    int getCount(){

        return count;

    }

    void setCount(int c){

        count = c;

    }

    string getZeroLine(){

        return zeroLine;

    }

    string getOpcode(){

        return opcode;

    }

    void setOpcode(string o){

        opcode = o;

    }

    string getOperand(){

        return operand;

    }

    void setOperand(string o){

        operand = o;

    }

private:

    string mem[32];
    string acc;
    int count;
    string zeroLine = "00000000000000000000000000000000";
    string opcode, operand;


};


#endif // BABY_H_
