##
# Manchester Baby
#
# @file
# @version 3

CC = g++
CFLAGS = -Wpedantic -g

all:
	$(CC) $(CFLAGS) baby.cpp baby.h -o baby

# end
