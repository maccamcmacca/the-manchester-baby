#include "baby.h"

using namespace std;

baby::baby(){

}

baby::~baby(){

}

int binToDec(string bin){

	//declare variables
	int dec = 0;
	int temp = 0;
	int base = 1;
	int binSize = bin.length();

	//convert binary to decimal
	for(int i = 0; i < binSize-1; i++){
		temp = bin[i] - '0';
		dec = dec + temp*base;
		base = base*2;
	}

	//determine if negative
	if(bin[binSize-1] == '1'){dec = -dec;}
	return dec;
}

//int baby::binToDec(long long n){
//
//    int decimalNumber = 0, i = 0, remainder;
//    while (n!=0)
//    {
//        remainder = n%10;
//        n /= 10;
//        decimalNumber += remainder*pow(2,i);
//        ++i;
//    }
//    return decimalNumber;
//}

string baby::dectoBin(int dec){
    string bin = "";	//binary value
	int rem;			//remainder
	int decTemp = dec;	//temp decimal value

	int binSize; 		//size of bin value

	//make decimal positive for calculation
	if(dec < 0){
		decTemp = -decTemp;
	}

	//loop until full binary number has been created
	while(decTemp != 0)
	{
		rem = decTemp%2;
		decTemp = decTemp/2;
		if(rem == 0){
			bin = bin + '0';
		}
		else{
			bin = bin + '1';
		}
	}

	//take into account twos compliment
	if(dec < 0){
		bin = bin + '1';
	}
	else{
		bin = bin + '0';
	}
	return bin;
}

void baby::initialise(){

    setAcc(getZeroLine());
    for(int i = 0; i < 32; i++){
        setMem(getZeroLine(), i);
    }

}

int getLittleEndian(string operand){

    int convertedOperand = 0;
    int counter = 1;
    bool leftoverbit = false;

    cout << operand.length();

    if(operand[0] == '1')
        leftoverbit = true;

    for(int i = 0; i < operand.length()-1; i++){

        counter+=counter;

        if(operand[i+1] == '1'){
            convertedOperand += counter;
        }

    }

    if(leftoverbit == true)
        convertedOperand+=1;

    return convertedOperand;
}

int getLittleEndianOperand(string operand){

    int convertedOperand = 0;
    int counter = 1;
    bool leftoverbit = false;


    if(operand[0] == '1')
        leftoverbit = true;

    for(int i = 0; i < operand.length(); i++){

        counter+=counter;

        if(operand[i+1] == '1'){
            convertedOperand += counter;
        }

    }

    if(leftoverbit == true)
        convertedOperand+=1;

    return convertedOperand;
}

void baby::fetch(){

    for(int i = 0; i < 32; i++){
        setOperand(getMem(i).substr(0, 12));
        setOpcode(getMem(i).substr(13, 15));
        cout << endl << getOperand() + " " << binToDec(getOperand()) << " " << getOpcode() <<  " Line " << i+1 <<  endl;
    }
    cout << endianSwap(getMem(17));
}

void baby::printMem(){

    cout << endl;
    for(int i = 0; i < 32; i++){

        cout << getMem(i) << " Line " << i+1 << endl;

    }
    cout << endl;

}

void baby::loadFile(string filename){

    fstream infile;
    infile.open(filename, ios::in);
    if(infile.is_open()){
        int lineCount = 0;
        string fl;
        while(getline(infile, fl)){
            setMem(fl, lineCount);

            lineCount += 1;

        }

    }

}

string baby::endianSwap(string binaryValue){

    string swappedEndian = binaryValue;

    for (int i=0; i < binaryValue.length(); i++){
        if (binaryValue[i] == '0'){

            swappedEndian[31 - i] = '0';

        }else{

            swappedEndian[31 - i] = '1';

        }
    }
    return swappedEndian;
}

int main(int argc, char** argv){

    baby thebaby;
    thebaby.initialise();
    thebaby.loadFile("jobby");
    thebaby.fetch();

}
